#![deny(clippy::all)]

mod engine;
mod game_objects;

use crate::{engine::Engine, game_objects::triangle::TriangleGameObject};

fn main() {
    Engine::new(720, 480, || vec![TriangleGameObject::new()]).start();
}
