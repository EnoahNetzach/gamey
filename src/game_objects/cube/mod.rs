use std::collections::HashMap;
use std::path::{Path, PathBuf};
use std::time::Duration;

use winit::event::VirtualKeyCode;

use crate::engine::game_object::{GameObject, Vertex};

fn create_vertices() -> (Vec<Vertex>, Vec<u16>) {
    let vertex_data = [
        // top (0, 0, 1)
        Vertex::new([-1, -1, 1], [0, 0]),
        Vertex::new([1, -1, 1], [1, 0]),
        Vertex::new([1, 1, 1], [1, 1]),
        Vertex::new([-1, 1, 1], [0, 1]),
        // bottom (0, 0, -1)
        Vertex::new([-1, 1, -1], [1, 0]),
        Vertex::new([1, 1, -1], [0, 0]),
        Vertex::new([1, -1, -1], [0, 1]),
        Vertex::new([-1, -1, -1], [1, 1]),
        // right (1, 0, 0)
        Vertex::new([1, -1, -1], [0, 0]),
        Vertex::new([1, 1, -1], [1, 0]),
        Vertex::new([1, 1, 1], [1, 1]),
        Vertex::new([1, -1, 1], [0, 1]),
        // left (-1, 0, 0)
        Vertex::new([-1, -1, 1], [1, 0]),
        Vertex::new([-1, 1, 1], [0, 0]),
        Vertex::new([-1, 1, -1], [0, 1]),
        Vertex::new([-1, -1, -1], [1, 1]),
        // front (0, 1, 0)
        Vertex::new([1, 1, -1], [1, 0]),
        Vertex::new([-1, 1, -1], [0, 0]),
        Vertex::new([-1, 1, 1], [0, 1]),
        Vertex::new([1, 1, 1], [1, 1]),
        // back (0, -1, 0)
        Vertex::new([1, -1, 1], [0, 0]),
        Vertex::new([-1, -1, 1], [1, 0]),
        Vertex::new([-1, -1, -1], [1, 1]),
        Vertex::new([1, -1, -1], [0, 1]),
    ];

    let index_data: &[u16] = &[
        0, 1, 2, 2, 3, 0, // top
        4, 5, 6, 6, 7, 4, // bottom
        8, 9, 10, 10, 11, 8, // right
        12, 13, 14, 14, 15, 12, // left
        16, 17, 18, 18, 19, 16, // front
        20, 21, 22, 22, 23, 20, // back
    ];

    (vertex_data.to_vec(), index_data.to_vec())
}

fn create_texels(size: usize) -> Vec<u8> {
    use std::iter;

    (0..size * size)
        .flat_map(|id| {
            // get high five for recognizing this ;)
            let cx = 3.0 * (id % size) as f32 / (size - 1) as f32 - 2.0;
            let cy = 2.0 * (id / size) as f32 / (size - 1) as f32 - 1.0;
            let (mut x, mut y, mut count) = (cx, cy, 0);
            while count < 0xFF && x * x + y * y < 4.0 {
                let old_x = x;
                x = x * x - y * y + cx;
                y = 2.0 * old_x * y + cy;
                count += 1;
            }
            iter::once(0xFF - (count * 5) as u8)
                .chain(iter::once(0xFF - (count * 15) as u8))
                .chain(iter::once(0xFF - (count * 50) as u8))
                .chain(iter::once(1))
        })
        .collect()
}

#[derive(Debug)]
pub struct CubeGameObject {}

impl CubeGameObject {
    pub fn new() -> CubeGameObject {
        CubeGameObject {}
    }
}

unsafe impl Send for CubeGameObject {}

unsafe impl Sync for CubeGameObject {}

impl GameObject for CubeGameObject {
    fn get_vert_shader(&self) -> PathBuf {
        Path::new("src")
            .join("game_objects")
            .join("triangle")
            .join("shader.vert")
    }

    fn get_frag_shader(&self) -> PathBuf {
        Path::new("src")
            .join("game_objects")
            .join("triangle")
            .join("shader.frag")
    }

    fn update(&mut self, pressed_keys: &HashMap<VirtualKeyCode, bool>, position: &mut (i32, i32), _dt: Duration) {
        if *pressed_keys.get(&VirtualKeyCode::A).unwrap_or(&false) {
            position.0 -= 100;
        } else if *pressed_keys.get(&VirtualKeyCode::D).unwrap_or(&false) {
            position.0 += 100;
        }

        if *pressed_keys.get(&VirtualKeyCode::W).unwrap_or(&false) {
            position.1 -= 100;
        } else if *pressed_keys.get(&VirtualKeyCode::S).unwrap_or(&false) {
            position.1 += 100;
        }
    }

    fn is_alive(&self) -> bool {
        true
    }
}
