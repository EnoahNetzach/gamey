use std::collections::HashMap;
use std::path::{Path, PathBuf};
use std::time::Duration;

use winit::event::VirtualKeyCode;

use crate::engine::game_object::GameObject;

#[derive(Debug)]
pub struct TriangleGameObject {
    prev_position: (i32, i32),
}

impl TriangleGameObject {
    pub fn new() -> TriangleGameObject {
        TriangleGameObject { prev_position: (0, 0) }
    }
}

unsafe impl Send for TriangleGameObject {}

unsafe impl Sync for TriangleGameObject {}

impl GameObject for TriangleGameObject {
    fn get_vert_shader(&self) -> PathBuf {
        Path::new("src")
            .join("game_objects")
            .join("triangle")
            .join("shader.vert")
    }

    fn get_frag_shader(&self) -> PathBuf {
        Path::new("src")
            .join("game_objects")
            .join("triangle")
            .join("shader.frag")
    }

    fn update(&mut self, pressed_keys: &HashMap<VirtualKeyCode, bool>, position: &mut (i32, i32), _dt: Duration) {
        if *pressed_keys.get(&VirtualKeyCode::A).unwrap_or(&false) {
            position.0 -= 100;
        } else if *pressed_keys.get(&VirtualKeyCode::D).unwrap_or(&false) {
            position.0 += 100;
        }

        if *pressed_keys.get(&VirtualKeyCode::W).unwrap_or(&false) {
            position.1 -= 100;
        } else if *pressed_keys.get(&VirtualKeyCode::S).unwrap_or(&false) {
            position.1 += 100;
        }
    }

    fn is_alive(&self) -> bool {
        true
    }
}
