#![deny(clippy::all)]
#![forbid(unsafe_code)]

#[derive(Debug, Clone, Copy)]
pub enum GameEvent {
    StartFrame,
    Exit,
}

#[derive(Debug, Clone, Copy)]
pub enum DrawEvent {
    GameSync,
    Resize(u32, u32),
    Exit,
}
