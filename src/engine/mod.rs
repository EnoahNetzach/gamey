#![deny(clippy::all)]

mod events;
pub mod game_object;
mod render_pass;
mod shader_compiler;
mod workers;

use std::borrow::{Borrow, BorrowMut};
use std::fs::{self, File};
use std::io::Read;
use std::path::Path;

use pixels::{wgpu::Surface, PixelsBuilder, SurfaceTexture};
use winit::{
    event::{ElementState, Event, KeyboardInput, StartCause, WindowEvent},
    event_loop::{ControlFlow, EventLoop},
    window::WindowBuilder,
};

use crate::engine::{
    game_object::GameObject, render_pass::RenderPass, shader_compiler::ShaderCompiler, workers::Workers,
};

pub struct Engine<G: GameObject + Send + Sync + 'static> {
    height: u32,
    game_objects_shaders: Vec<(Vec<u8>, Vec<u8>)>,
    get_game_objects: fn() -> Vec<G>,
    width: u32,
}

impl<G: GameObject + Send + Sync + 'static> Engine<G> {
    pub fn new(width: u32, height: u32, get_game_objects: fn() -> Vec<G>) -> Self {
        let mut shader_compiler = ShaderCompiler::new();

        let game_objects_shaders = get_game_objects()
            .iter()
            .map(|game_object| {
                let vert_shader = shader_compiler.compile(&game_object.get_vert_shader(), None).unwrap();
                let frag_shader = shader_compiler.compile(&game_object.get_frag_shader(), None).unwrap();

                (vert_shader, frag_shader)
            })
            .collect();

        Engine {
            game_objects_shaders,
            get_game_objects,
            height,
            width,
        }
    }

    pub fn start(&mut self) {
        let event_loop = EventLoop::new();
        let window = WindowBuilder::new().with_title("Gamey").build(&event_loop).unwrap();
        let surface = Surface::create(&window);
        let surface_texture = SurfaceTexture::new(self.width, self.height, surface);

        let mut pixel_buffer_builder = PixelsBuilder::new(self.width, self.height, surface_texture);

        for game_object_shaders in self.game_objects_shaders.clone() {
            pixel_buffer_builder = pixel_buffer_builder.add_render_pass(move |device, queue, _, _| {
                RenderPass::factory(
                    game_object_shaders.0.as_slice(),
                    game_object_shaders.1.as_slice(),
                    device,
                    queue,
                )
            });
        }

        let mut pixel_buffer = pixel_buffer_builder.build().unwrap();

        let mut workers = Workers::new(
            self.height,
            self.width,
            pixel_buffer.borrow_mut(),
            self.get_game_objects,
        );

        event_loop.run(move |event, _, control_flow| {
            *control_flow = ControlFlow::Wait;

            match event {
                Event::NewEvents(StartCause::Init) => workers.start(),
                Event::WindowEvent {
                    event: WindowEvent::CloseRequested,
                    ..
                } => {
                    workers.exit();

                    *control_flow = ControlFlow::Exit
                }
                Event::WindowEvent {
                    event: WindowEvent::Resized(size),
                    ..
                } => workers.resize(size.width, size.height),
                Event::RedrawRequested { .. } => {}
                Event::WindowEvent {
                    event:
                        WindowEvent::KeyboardInput {
                            input:
                                KeyboardInput {
                                    virtual_keycode: Some(virtual_code),
                                    state: ElementState::Pressed,
                                    ..
                                },
                            ..
                        },
                    ..
                } => workers.set_key_pressed(virtual_code),
                _ => (),
            }
        });
    }
}
