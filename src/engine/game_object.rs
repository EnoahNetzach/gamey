#![deny(clippy::all)]
#![forbid(unsafe_code)]

use std::collections::HashMap;
use std::path::PathBuf;
use std::time::Duration;

use winit::event::VirtualKeyCode;
use zerocopy::{AsBytes, FromBytes};

#[repr(C)]
#[derive(Clone, Copy, AsBytes, FromBytes)]
pub struct Vertex {
    _pos: [f32; 4],
    _tex_coord: [f32; 2],
}

impl Vertex {
    pub fn new(pos: [i8; 3], tc: [i8; 2]) -> Self {
        Vertex {
            _pos: [pos[0] as f32, pos[1] as f32, pos[2] as f32, 1.0],
            _tex_coord: [tc[0] as f32, tc[1] as f32],
        }
    }
}

pub trait GameObject {
    fn get_vert_shader(&self) -> PathBuf;

    fn get_frag_shader(&self) -> PathBuf;

    fn init(&mut self) {}

    fn update(&mut self, _pressed_keys: &HashMap<VirtualKeyCode, bool>, _position: &mut (i32, i32), _dt: Duration) {}

    fn is_alive(&self) -> bool;

    fn spawned_game_objects<G: GameObject>(&mut self) -> Vec<G> {
        Vec::new()
    }
}
