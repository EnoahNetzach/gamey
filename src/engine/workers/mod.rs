pub mod game;
pub mod render;

use std::borrow::{Borrow, BorrowMut};
use std::collections::HashMap;
use std::sync::atomic::AtomicPtr;
use std::sync::{mpsc, Arc, RwLock};
use std::thread::JoinHandle;

use pixels::Pixels;
use winit::event::VirtualKeyCode;

use crate::engine::{
    events::{DrawEvent, GameEvent},
    game_object::GameObject,
    workers::{game::thread as game_thread, render::thread as render_thread},
};

pub struct Workers<G: GameObject + Send + Sync + 'static> {
    atomic_pixel_buffer: Arc<AtomicPtr<Pixels>>,
    render_handle: Option<JoinHandle<()>>,
    height: Arc<RwLock<u32>>,
    game_handle: Option<JoinHandle<()>>,
    get_game_objects: fn() -> Vec<G>,
    pressed_keys: Arc<RwLock<HashMap<VirtualKeyCode, bool>>>,
    sender_for_render: Option<mpsc::Sender<DrawEvent>>,
    sender_for_game: Option<mpsc::Sender<GameEvent>>,
    width: Arc<RwLock<u32>>,
}

impl<G: GameObject + Send + Sync + 'static> Workers<G> {
    pub fn new(
        initial_height: u32,
        initial_width: u32,
        pixel_buffer: &mut Pixels,
        get_game_objects: fn() -> Vec<G>,
    ) -> Self {
        let atomic_pixel_buffer = Arc::new(AtomicPtr::new(pixel_buffer.borrow_mut()));

        let pressed_keys = Arc::new(RwLock::new(HashMap::new()));

        let width = Arc::new(RwLock::new(initial_width));
        let height = Arc::new(RwLock::new(initial_height));

        Workers {
            atomic_pixel_buffer,
            render_handle: None,
            height,
            game_handle: None,
            get_game_objects,
            pressed_keys,
            sender_for_render: None,
            sender_for_game: None,
            width,
        }
    }

    pub fn start(&mut self) {
        let (sender_for_game, game_receiver) = mpsc::channel::<GameEvent>();
        let (sender_for_render, render_receiver) = mpsc::channel::<DrawEvent>();

        self.sender_for_game = Some(sender_for_game.clone());
        self.sender_for_render = Some(sender_for_render.clone());

        self.game_handle = Some(game_thread(
            self.get_game_objects,
            self.pressed_keys.clone(),
            game_receiver,
            sender_for_render,
        ));

        self.render_handle = Some(render_thread(
            self.width.clone(),
            self.height.clone(),
            self.atomic_pixel_buffer.clone(),
            render_receiver,
            sender_for_game.clone(),
        ));

        sender_for_game.send(GameEvent::StartFrame).ok();
    }

    pub fn exit(&mut self) {
        self.sender_for_game.take().map(|sender| sender.send(GameEvent::Exit));
        self.sender_for_render.take().map(|sender| sender.send(DrawEvent::Exit));

        self.game_handle.take().map(JoinHandle::join);
        self.render_handle.take().map(JoinHandle::join);
    }

    pub fn resize(&self, width: u32, height: u32) {
        if let Some(sender) = self.sender_for_render.borrow() {
            sender.send(DrawEvent::Resize(width, height)).ok();
        }
    }

    pub fn set_key_pressed(&self, key_code: VirtualKeyCode) {
        if let Ok(mut write_guard) = self.pressed_keys.write() {
            write_guard.insert(key_code, true);
        }
    }
}
