#![deny(clippy::all)]

use std::boxed::Box;
use std::sync::atomic::{AtomicPtr, Ordering};
use std::sync::{mpsc, Arc, RwLock};
use std::thread;
use std::time::{Duration, Instant};

use pixels::Pixels;

use crate::engine::events::{DrawEvent, GameEvent};

const DESIRED_FPS: u64 = 60;

pub fn thread(
    width: Arc<RwLock<u32>>,
    height: Arc<RwLock<u32>>,
    atomic_pixel_buffer: Arc<AtomicPtr<Pixels>>,
    receiver: mpsc::Receiver<DrawEvent>,
    sender_for_game: mpsc::Sender<GameEvent>,
) -> thread::JoinHandle<()> {
    thread::spawn(move || {
        let mut pixel_buffer = unsafe { Box::from_raw(atomic_pixel_buffer.load(Ordering::Relaxed)) };

        let mut requested_width = 0;
        let mut requested_height = 0;
        let mut resize_requested = false;

        let timer_length = Duration::from_micros(1_000_000 / DESIRED_FPS);
        let mut frame_start_time = Instant::now();
        let mut current_time_frame_budget = timer_length;
        const ZERO_DURATION: Duration = Duration::from_micros(0);

        loop {
            match receiver.recv() {
                Ok(DrawEvent::GameSync) => {
                    current_time_frame_budget = current_time_frame_budget
                        .checked_sub(Instant::now().duration_since(frame_start_time))
                        .unwrap_or(ZERO_DURATION);

                    if current_time_frame_budget > Duration::new(0, 0) {
                        thread::sleep(current_time_frame_budget);
                    }

                    let fps = 1_000_000 / Instant::now().duration_since(frame_start_time).as_micros();

                    println!("{:?}fps", fps);

                    if resize_requested {
                        pixel_buffer.resize(requested_width, requested_height);
                        *width.write().unwrap() = requested_width;
                        *height.write().unwrap() = requested_height;
                        resize_requested = false;
                    }

                    // Restart the frame timer and reset the time budget
                    frame_start_time = Instant::now();
                    current_time_frame_budget = timer_length;

                    // Next tick of the game and render loops
                    sender_for_game.send(GameEvent::StartFrame).ok();

                    pixel_buffer.render();
                }
                Ok(DrawEvent::Resize(width, height)) => {
                    requested_width = width;
                    requested_height = height;
                    resize_requested = true;
                }
                Ok(DrawEvent::Exit) => {
                    println!("Draw Loop Exit");
                    break;
                }
                _ => {}
            }
        }
    })
}
