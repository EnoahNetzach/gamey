#![deny(clippy::all)]
#![forbid(unsafe_code)]

use std::borrow::Borrow;
use std::collections::HashMap;
use std::iter;
use std::ops::Deref;
use std::sync::{mpsc, Arc, RwLock};
use std::thread;
use std::time::Instant;

use rand::{distributions::Alphanumeric, thread_rng, Rng};
use winit::event::VirtualKeyCode;

use crate::engine::{
    events::{DrawEvent, GameEvent},
    game_object::GameObject,
};

pub fn thread<G: GameObject + Send + Sync + 'static>(
    get_game_objects: fn() -> Vec<G>,
    rw_lock_pressed_keys: Arc<RwLock<HashMap<VirtualKeyCode, bool>>>,
    receiver: mpsc::Receiver<GameEvent>,
    sender_for_render: mpsc::Sender<DrawEvent>,
) -> thread::JoinHandle<()> {
    thread::spawn(move || {
        let mut rng = thread_rng();

        let mut game_object_hash = || iter::repeat(()).map(|()| rng.sample(Alphanumeric)).take(7).collect();

        let mut position: (i32, i32) = (42, 42);
        let mut time = Instant::now();

        let mut game_objects =
            get_game_objects()
                .into_iter()
                .fold(HashMap::<String, G>::new(), |mut all, mut game_object| {
                    game_object.init();
                    all.insert(game_object_hash(), game_object);
                    all
                });

        loop {
            match receiver.recv() {
                Ok(GameEvent::StartFrame) => {
                    // Get a new delta time.
                    let now = Instant::now();
                    let dt = now.duration_since(time);
                    time = now;

                    // Extract the pressed keys so far, and clear the shared hashmap
                    let mut pressed_keys = HashMap::<VirtualKeyCode, bool>::new();
                    {
                        pressed_keys.clone_from(rw_lock_pressed_keys.read().unwrap().deref());
                    }
                    {
                        rw_lock_pressed_keys.write().unwrap().clear();
                    }

                    let mut next_game_objects = HashMap::<String, G>::new();

                    for (hash, mut game_object) in game_objects {
                        game_object.update(pressed_keys.borrow(), &mut position, dt);

                        for mut spawned_game_object in game_object.spawned_game_objects::<G>() {
                            spawned_game_object.init();
                            next_game_objects.insert(game_object_hash(), spawned_game_object);
                        }

                        if game_object.is_alive() {
                            next_game_objects.insert(hash, game_object);
                        }
                    }

                    game_objects = next_game_objects;

                    sender_for_render.send(DrawEvent::GameSync).ok();
                }
                Ok(GameEvent::Exit) => {
                    println!("Game Loop Exit");
                    return;
                }
                _ => {}
            }
        }
    })
}
