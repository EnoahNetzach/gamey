#![deny(clippy::all)]
#![forbid(unsafe_code)]

use std::error::Error;
use std::ffi::OsStr;
use std::fmt;
use std::fs::read_to_string;
use std::fs::File;
use std::path::Path;
use std::result::Result;

use shaderc;

#[derive(Debug)]
struct NoError {}

impl fmt::Display for NoError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "")
    }
}

impl Error for NoError {}

#[derive(Debug)]
struct BuildError<E: Error> {
    cause: Option<E>,
    message: String,
}

impl BuildError<NoError> {
    pub fn new(message: String) -> Self {
        BuildError { cause: None, message }
    }
}

impl<E: Error> BuildError<E> {
    pub fn from(message: String, cause: E) -> Self {
        BuildError {
            cause: Some(cause),
            message,
        }
    }
}

impl<E: Error> fmt::Display for BuildError<E> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match &self.cause {
            Some(cause) => write!(f, "{}: {}", self.message, cause),
            None => write!(f, "{}", self.message),
        }
    }
}

impl<E: Error> Error for BuildError<E> {}

pub struct ShaderCompiler {
    compiler: shaderc::Compiler,
}

impl ShaderCompiler {
    pub fn new() -> Self {
        let compiler = shaderc::Compiler::new().unwrap();

        ShaderCompiler { compiler }
    }

    pub fn compile(
        &mut self,
        shader_path: &Path,
        compiler_options: Option<shaderc::CompileOptions>,
    ) -> Result<Vec<u8>, Box<dyn Error>> {
        let mut compiler_options = compiler_options.or_else(shaderc::CompileOptions::new).unwrap();
        compiler_options.add_macro_definition("EP", Some("main"));

        let source = read_to_string(shader_path)
            .map_err(|e| BuildError::from(format!("Read file {}", shader_path.to_str().unwrap()), e))?;

        let target_kind = match shader_path.extension().and_then(OsStr::to_str) {
            Some("vert") => shaderc::ShaderKind::Vertex,
            Some("tesc") => shaderc::ShaderKind::TessControl,
            Some("tese") => shaderc::ShaderKind::TessEvaluation,
            Some("geom") => shaderc::ShaderKind::Geometry,
            Some("frag") => shaderc::ShaderKind::Fragment,
            Some("comp") => shaderc::ShaderKind::Compute,
            Some(ext) => {
                return Err(Box::new(BuildError::new(format!(
                    "Invalid extension {} for file {}",
                    ext,
                    shader_path.to_str().unwrap()
                ))))
            }
            _ => {
                return Err(Box::new(BuildError::new(format!(
                    "Unknown extension for file {}",
                    shader_path.to_str().unwrap()
                ))))
            }
        };

        let compiled = self
            .compiler
            .compile_into_spirv(
                source.as_str(),
                target_kind,
                shader_path.to_str().unwrap(),
                "main",
                Some(&compiler_options),
            )
            .map_err(|e| {
                Box::new(BuildError::from(
                    format!("Compile file {}", shader_path.to_str().unwrap()),
                    e,
                )) as Box<dyn Error>
            })?;

        Ok(Vec::from(compiled.as_binary_u8()))
    }
}

// fn collect_files_in_dir(dir: &Path) -> io::Result<Vec<PathBuf>> {
//     fs::read_dir(dir).map(|res| {
//         res.filter_map(Result::ok)
//             .flat_map(|entry| {
//                 let sub_path = entry.path();
//                 if sub_path.is_dir() {
//                     collect_files_in_dir(&sub_path).unwrap()
//                 } else if sub_path.is_file() {
//                     vec![sub_path]
//                 } else {
//                     Vec::new()
//                 }
//             })
//             .collect::<HashSet<_>>()
//             .into_iter()
//             .collect()
//     })
// }

// pub fn compile_shaders(source_path: &Path, target_path: &Path) -> io::Result<Vec<(PathBuf, shaderc::ShaderKind)>> {
//     let assets_path = "shaders";
//     let source_assets_path = source_path.join(assets_path);
//     let target_assets_path = target_path.join(assets_path);
//
//     let assets = collect_files_in_dir(source_assets_path.as_path())?;
//
//     let shaders = assets
//         .iter()
//         .filter(|path| match path.extension().and_then(OsStr::to_str) {
//             Some("vert") | Some("tesc") | Some("tese") | Some("geom") | Some("frag") | Some("comp") => true,
//             _ => false,
//         })
//         .collect::<Vec<_>>();
//
//     let mut compiler = shaderc::Compiler::new().unwrap();
//     let mut options = shaderc::CompileOptions::new().unwrap();
//     options.add_macro_definition("EP", Some("main"));
//
//     let mut target_paths = Vec::new();
//
//     for shader in shaders {
//         match compile_shader(
//             &mut compiler,
//             &options,
//             &shader,
//             &source_assets_path,
//             &target_assets_path,
//         ) {
//             Ok(target_path) => target_paths.push(target_path),
//             Err(err) => println!(
//                 "Error while compiling shader {}: {}",
//                 shader.to_str().unwrap(),
//                 err.to_string()
//             ),
//         }
//     }
//
//     Ok(target_paths)
// }
